# README #

Retriever4 is a fourth iteration of a Retriever series programme written to help with comparing computer parameters with desired ones. Language of the application is Polish.

Note: I know that this is an architectonic and design nightmare and that the tests are not passing, I had a quick view through the files and for sure I would wirte it better these days, but this was a milestone for me and it did its purpose, so I decided to leave it public.

Without particular MS Excel file application will not start, so if you require to see how it works live - let me know. Please find screenshots below.

Preview of model selection screen:
![Model selection](Blob/ModelSelection.png)

Preview of comparison results:
![Data comparison results](Blob/DataComparison.png)

### How do I get set up? ###

* Prerequisites: installed .NET Framework 4.5.2 Runtime, installed Visual Studio, a specific Excel file containing data to compare with,
* Download as ZIP or clone repository to your local machine,
* Open Retriever4.sln,
* Run and enjoy!

### Additional packages used in the project ###
* [NUnit](https://nunit.org/)
* [Colorful.Console](https://github.com/tomakita/Colorful.Console)
* [ExcelDataReader](https://github.com/ExcelDataReader/ExcelDataReader)
